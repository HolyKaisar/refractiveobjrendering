#include "DXUT.h"
#include "VoxelViewer.h"


VoxelViewer::VoxelViewer()
{
	m_pVertexShader = nullptr;
	m_pGeometryShader = nullptr;
	m_pPixelShader = nullptr;
	m_pVoxelViewLayout = nullptr;
	m_pFreqUpdateBuffer = nullptr;
	m_pSamplerLinear = nullptr;

	m_pRTVTex2D = nullptr;
	m_pRTVSRV = nullptr;
	m_pRTV = nullptr;

	m_pDepthTex2D = nullptr;
	m_pDepthStencilState = nullptr;
	m_pDepthStencilView = nullptr;

	m_pWireFrameRS = nullptr;
	m_pSolidStateRS = nullptr;

	m_pModelVS = nullptr;
	m_pModelPS = nullptr;
	m_pModelLayout = nullptr;

	m_pModelTex2D = nullptr;
	m_pModelSRV = nullptr;
	m_pModelRTV = nullptr;
}

VoxelViewer::~VoxelViewer()
{
	DestroyResource();
}

HRESULT VoxelViewer::CreateResource(ID3D11Device* pd3dDevice, uint32_t bbWidth, uint32_t bbHeight)
{
	HRESULT hr = S_OK;

	DWORD dwShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;

#ifdef _DEBUG
	// Set the D3DCOMPILE_DEBUG flag to embed debug information in the shaders.
	// Setting this flag improves the shader debugging experience, but still allows 
	// the shaders to be optimized and to run exactly the way they will run in 
	// the release configuration of this program.
	dwShaderFlags |= D3DCOMPILE_DEBUG;

	// Disable optimizations to further improve shader debugging
	dwShaderFlags |= D3DCOMPILE_SKIP_OPTIMIZATION;
#endif

	// Compile the vertex shader
	ID3DBlob* pVSBlob = nullptr;
	V_RETURN(DXUTCompileFromFile(L"VoxelView.hlsl", nullptr, "VS", "vs_5_0", dwShaderFlags, 0, &pVSBlob));

	// Create the vertex shader
	hr = pd3dDevice->CreateVertexShader(pVSBlob->GetBufferPointer(), pVSBlob->GetBufferSize(), nullptr, &m_pVertexShader);
	if (FAILED(hr))
	{
		SAFE_RELEASE(pVSBlob);
		return hr;
	}

	// Define the input layout
	D3D11_INPUT_ELEMENT_DESC layout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT , D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT , D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	UINT numElements = ARRAYSIZE(layout);

	// Create the input layout
	hr = pd3dDevice->CreateInputLayout(layout, numElements, pVSBlob->GetBufferPointer(),
		pVSBlob->GetBufferSize(), &m_pVoxelViewLayout);
	SAFE_RELEASE(pVSBlob);
	if (FAILED(hr))
		return hr;

	// Create VS for rendering model
	pVSBlob = nullptr;
	V_RETURN(DXUTCompileFromFile(L"VoxelView.hlsl", nullptr, "ModelVS", "vs_5_0", dwShaderFlags, 0, &pVSBlob));
	hr = pd3dDevice->CreateVertexShader(pVSBlob->GetBufferPointer(), pVSBlob->GetBufferSize(), nullptr, &m_pModelVS);
	if (FAILED(hr))
	{
		SAFE_RELEASE(pVSBlob);
		return hr;
	}

	// Create the input layout
	hr = pd3dDevice->CreateInputLayout(layout, numElements, pVSBlob->GetBufferPointer(),
		pVSBlob->GetBufferSize(), &m_pModelLayout);
	SAFE_RELEASE(pVSBlob);
	if (FAILED(hr))
		return hr;

	// Compile the geometry shader
	ID3DBlob * pGSBlob = nullptr;
	V_RETURN(DXUTCompileFromFile(L"VoxelView.hlsl", nullptr, "GS", "gs_5_0", dwShaderFlags, 0, &pGSBlob));

	// Create the geometry shader
	hr = pd3dDevice->CreateGeometryShader(pGSBlob->GetBufferPointer(), pGSBlob->GetBufferSize(), nullptr, &m_pGeometryShader);
	SAFE_RELEASE(pGSBlob);
	if (FAILED(hr))
		return hr;

	// Compile the pixel shader for rendering voxels
	ID3DBlob* pPSBlob = nullptr;
	V_RETURN(DXUTCompileFromFile(L"VoxelView.hlsl", nullptr, "PS", "ps_5_0", dwShaderFlags, 0, &pPSBlob));

	// Create the pixel shader
	hr = pd3dDevice->CreatePixelShader(pPSBlob->GetBufferPointer(), pPSBlob->GetBufferSize(), nullptr, &m_pPixelShader);
	SAFE_RELEASE(pPSBlob);
	if (FAILED(hr))
		return hr;

	// Compile the pixel shader for rendering model
	pPSBlob = nullptr;
	V_RETURN(DXUTCompileFromFile(L"VoxelView.hlsl", nullptr, "ModelPS", "ps_5_0", dwShaderFlags, 0, &pPSBlob));

	// Create the pixel shader
	hr = pd3dDevice->CreatePixelShader(pPSBlob->GetBufferPointer(), pPSBlob->GetBufferSize(), nullptr, &m_pModelPS);
	SAFE_RELEASE(pPSBlob);
	if (FAILED(hr))
		return hr;

	// Create Render Target
	D3D11_TEXTURE2D_DESC	RTtextureDesc = { 0 };
	RTtextureDesc.Width = bbWidth;
	RTtextureDesc.Height = bbHeight;
	RTtextureDesc.MipLevels = 1;
	RTtextureDesc.ArraySize = 1;
	RTtextureDesc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	RTtextureDesc.SampleDesc.Count = 1;
	RTtextureDesc.Usage = D3D11_USAGE_DEFAULT;
	RTtextureDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
	RTtextureDesc.CPUAccessFlags = 0;
	RTtextureDesc.MiscFlags = 0;
	V_RETURN(pd3dDevice->CreateTexture2D(&RTtextureDesc, NULL, &m_pRTVTex2D));
	V_RETURN(pd3dDevice->CreateTexture2D(&RTtextureDesc, NULL, &m_pModelTex2D));

	D3D11_SHADER_RESOURCE_VIEW_DESC RTshaderResourceDesc;
	RTshaderResourceDesc.Format = RTtextureDesc.Format;
	RTshaderResourceDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	RTshaderResourceDesc.Texture2D.MostDetailedMip = 0;
	RTshaderResourceDesc.Texture2D.MipLevels = 1;
	V_RETURN(pd3dDevice->CreateShaderResourceView(m_pRTVTex2D, &RTshaderResourceDesc, &m_pRTVSRV));
	V_RETURN(pd3dDevice->CreateShaderResourceView(m_pModelTex2D, &RTshaderResourceDesc, &m_pModelSRV));

	D3D11_RENDER_TARGET_VIEW_DESC	RTviewDesc;
	RTviewDesc.Format = RTtextureDesc.Format;
	RTviewDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
	RTviewDesc.Texture2D.MipSlice = 0;
	V_RETURN(pd3dDevice->CreateRenderTargetView(m_pRTVTex2D, &RTviewDesc, &m_pRTV));
	V_RETURN(pd3dDevice->CreateRenderTargetView(m_pModelTex2D, &RTviewDesc, &m_pModelRTV));

	// Create Stencil View
	D3D11_TEXTURE2D_DESC descDepth;
	ZeroMemory(&descDepth, sizeof(descDepth));
	descDepth.Width = bbWidth;
	descDepth.Height = bbHeight;
	descDepth.MipLevels = 1;
	descDepth.ArraySize = 1;
	descDepth.Format = DXUTGetDeviceSettings().d3d11.AutoDepthStencilFormat;
	descDepth.SampleDesc.Count = 1;
	descDepth.SampleDesc.Quality = 0;
	descDepth.Usage = D3D11_USAGE_DEFAULT;
	descDepth.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	descDepth.CPUAccessFlags = 0;
	descDepth.MiscFlags = 0;
	hr = pd3dDevice->CreateTexture2D(&descDepth, NULL, &m_pDepthTex2D);

	D3D11_DEPTH_STENCIL_VIEW_DESC descDSV;
	ZeroMemory(&descDSV, sizeof(descDSV));
	descDSV.Format = descDepth.Format;
	descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	descDSV.Texture2D.MipSlice = 0;

	// Create Depth Stencil State
	D3D11_DEPTH_STENCIL_DESC dsDesc;
	// Depth test parameters
	dsDesc.DepthEnable = true; // Disable Depth Test
	dsDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	dsDesc.DepthFunc = D3D11_COMPARISON_LESS;
	// Stencil test parameters
	dsDesc.StencilEnable = true;
	dsDesc.StencilReadMask = 0xFF;
	dsDesc.StencilWriteMask = 0xFF;
	// Stencil operations if pixel is front-facing
	dsDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	dsDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	// Stencil operations if pixel is back-facing
	dsDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	dsDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	V_RETURN(pd3dDevice->CreateDepthStencilState(&dsDesc, &m_pDepthStencilState));

	// Create the depth stencil view
	V_RETURN(pd3dDevice->CreateDepthStencilView(m_pDepthTex2D, // Depth stencil texture
		&descDSV, // Depth stencil desc
		&m_pDepthStencilView));

	m_viewPort.TopLeftX = 0;
	m_viewPort.TopLeftY = 0;
	m_viewPort.MinDepth = 0.0f;
	m_viewPort.MaxDepth = 1.0f;
	m_viewPort.Width = bbWidth;
	m_viewPort.Height = bbHeight;

	// Setup wire frame mode
	// Create No Back Culling RS;
	D3D11_RASTERIZER_DESC rasterizerState;
	rasterizerState.FillMode = D3D11_FILL_WIREFRAME;
	rasterizerState.CullMode = D3D11_CULL_NONE;
	rasterizerState.FrontCounterClockwise = true;
	rasterizerState.DepthBias = false;
	rasterizerState.DepthBiasClamp = 0;
	rasterizerState.SlopeScaledDepthBias = 0;
	rasterizerState.DepthClipEnable = true;
	rasterizerState.ScissorEnable = false;
	rasterizerState.MultisampleEnable = true;
	rasterizerState.AntialiasedLineEnable = false;
	V_RETURN(pd3dDevice->CreateRasterizerState(&rasterizerState, &m_pWireFrameRS));
	rasterizerState.FillMode = D3D11_FILL_SOLID;
	rasterizerState.CullMode = D3D11_CULL_NONE;
	V_RETURN(pd3dDevice->CreateRasterizerState(&rasterizerState, &m_pSolidStateRS));

	// Setup Model Based Camera
	// Setup the camera's view parameters
	static const XMVECTORF32 s_Eye = { 0.0f, 0.0f, -6.0f, 0.f };
	static const XMVECTORF32 s_At = { 0.0f, 0.0f, 0.0f, 0.f };
	m_modelViewCamera.SetViewParams(s_Eye, s_At);
	// Setup the camera's projection parameters
	float fAspectRatio = bbWidth / (float)bbHeight;
	m_modelViewCamera.SetProjParams(XM_PI / 4, fAspectRatio, 0.01f, 1000.0f);
	m_modelViewCamera.SetWindow(bbWidth, bbHeight);
	m_modelViewCamera.SetButtonMasks(MOUSE_LEFT_BUTTON, MOUSE_WHEEL, MOUSE_MIDDLE_BUTTON);

	m_WorldMatrix = XMMatrixIdentity();

	// Create the constant buffers
	D3D11_BUFFER_DESC bd;
	bd.Usage = D3D11_USAGE_DYNAMIC;
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	bd.ByteWidth = sizeof(FreqUpdatedCB);
	bd.MiscFlags = 0;
	V_RETURN(pd3dDevice->CreateBuffer(&bd, nullptr, &m_pFreqUpdateBuffer));

	return hr;
}

void VoxelViewer::DestroyResource()
{
	SAFE_RELEASE(m_pVertexShader);
	SAFE_RELEASE(m_pGeometryShader);
	SAFE_RELEASE(m_pPixelShader);
	SAFE_RELEASE(m_pVoxelViewLayout);
	SAFE_RELEASE(m_pFreqUpdateBuffer);
	SAFE_RELEASE(m_pSamplerLinear);

	SAFE_RELEASE(m_pRTVTex2D);
	SAFE_RELEASE(m_pRTVSRV);
	SAFE_RELEASE(m_pRTV);

	SAFE_RELEASE(m_pDepthTex2D);
	SAFE_RELEASE(m_pDepthStencilState);
	SAFE_RELEASE(m_pDepthStencilView);

	SAFE_RELEASE(m_pWireFrameRS);
	SAFE_RELEASE(m_pSolidStateRS);

	SAFE_RELEASE(m_pModelVS);
	SAFE_RELEASE(m_pModelPS);
	SAFE_RELEASE(m_pModelLayout);

	SAFE_RELEASE(m_pModelTex2D);
	SAFE_RELEASE(m_pModelSRV);
	SAFE_RELEASE(m_pModelRTV);
}

void VoxelViewer::RenderVoxels(ID3D11DeviceContext* pd3dImmediateContext, ID3D11ShaderResourceView** inputSRV, 
								uint32_t& vWidth, uint32_t& vHeight, uint32_t& vDepth, XMFLOAT4& volumeScales)
{
	ID3D11RasterizerState* rs = nullptr;
	pd3dImmediateContext->RSGetState(&rs);

	//pd3dImmediateContext->ClearRenderTargetView(m_pRTV, Colors::MidnightBlue);
	//pd3dImmediateContext->OMSetRenderTargets(1, &m_pRTV, nullptr);

	pd3dImmediateContext->RSSetViewports(1, &m_viewPort);
	// Set the input layout
	ID3D11InputLayout* ppNullLayout = nullptr;
	//pd3dImmediateContext->IASetInputLayout(m_pVoxelViewLayout);
	pd3dImmediateContext->IASetInputLayout(ppNullLayout);
	// Set primitive topology
	pd3dImmediateContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);
	pd3dImmediateContext->RSSetState(m_pWireFrameRS);

	XMMATRIX mWorldViewProjection = m_WorldMatrix * m_modelViewCamera.GetViewMatrix() * m_modelViewCamera.GetProjMatrix();
	//XMMATRIX mWorldViewProjection = XMMatrixIdentity();
	HRESULT hr;
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	V(pd3dImmediateContext->Map(m_pFreqUpdateBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource));
	auto pCB = reinterpret_cast<FreqUpdatedCB*>(mappedResource.pData);
	XMStoreFloat4x4(&pCB->mWorldViewProj, XMMatrixTranspose(mWorldViewProjection));
	pCB->vVolumeDimension = XMUINT4(vWidth, vHeight, vDepth, 1);
	pCB->vVolumeDimensionScale = volumeScales;
	pCB->vMeshColor = XMFLOAT4(1.0f, 0.0f, 1.0f, 1.0f);
	pd3dImmediateContext->Unmap(m_pFreqUpdateBuffer, 0);

	// Render the cube
	pd3dImmediateContext->VSSetShader(m_pVertexShader, nullptr, 0);
	pd3dImmediateContext->VSSetConstantBuffers(0, 1, &m_pFreqUpdateBuffer);
	pd3dImmediateContext->VSSetShaderResources(0, 1, inputSRV);
	pd3dImmediateContext->GSSetShader(m_pGeometryShader, nullptr, 0);
	pd3dImmediateContext->GSSetConstantBuffers(0, 1, &m_pFreqUpdateBuffer);
	pd3dImmediateContext->PSSetShader(m_pPixelShader, nullptr, 0);
	//pd3dImmediateContext->PSSetConstantBuffers(0, 1, &m_pFreqUpdateBuffer);
	pd3dImmediateContext->PSSetSamplers(0, 1, &m_pSamplerLinear);
	uint32_t vertexCount = vWidth * vHeight * vDepth;
	pd3dImmediateContext->Draw(vertexCount, 0);

	ID3D11VertexShader* tmpVertexShader = nullptr;
	ID3D11PixelShader*  tmpPixelShader = nullptr;
	ID3D11GeometryShader*  tmpGeometryShader = nullptr;
	ID3D11Buffer*		tmpBuffer = nullptr;
	ID3D11ShaderResourceView* tmpSRV = nullptr;
	ID3D11SamplerState* tmpSampler = nullptr;
	pd3dImmediateContext->VSSetShader(tmpVertexShader, nullptr, 0);
	pd3dImmediateContext->VSSetConstantBuffers(0, 1, &tmpBuffer);
	pd3dImmediateContext->VSSetShaderResources(0, 1, &tmpSRV);
	pd3dImmediateContext->GSSetShader(tmpGeometryShader, nullptr, 0);
	pd3dImmediateContext->PSSetShader(tmpPixelShader, nullptr, 0);
	pd3dImmediateContext->PSSetConstantBuffers(0, 1, &tmpBuffer);
	pd3dImmediateContext->PSSetSamplers(0, 1, &tmpSampler);

	// Restore the blend state
	pd3dImmediateContext->OMSetBlendState(0, 0, 0xffffffff);
	pd3dImmediateContext->RSSetState(rs);
	SAFE_RELEASE(rs);
}

void VoxelViewer::RenderModel(ID3D11DeviceContext* pd3dImmediateContext, ObjModelViewer* objModel)
{
	ID3D11RasterizerState* rs = nullptr;
	pd3dImmediateContext->RSGetState(&rs);

	pd3dImmediateContext->ClearRenderTargetView(m_pModelRTV, Colors::MidnightBlue);
	pd3dImmediateContext->ClearDepthStencilView(m_pDepthStencilView, D3D11_CLEAR_DEPTH, 1.0, 0);
	pd3dImmediateContext->OMSetRenderTargets(1, &m_pModelRTV, m_pDepthStencilView);
	pd3dImmediateContext->OMSetDepthStencilState(m_pDepthStencilState, 1);

	pd3dImmediateContext->RSSetViewports(1, &m_viewPort);
	// Set the input layout
	pd3dImmediateContext->IASetInputLayout(m_pModelLayout);
	// Set vertex buffer/
	UINT stride = objModel->GetVertexStride();
	UINT offset = 0;
	pd3dImmediateContext->IASetVertexBuffers(0, 1, objModel->GetVertexBuffer(), &stride, &offset);
	// Set index buffer
	pd3dImmediateContext->IASetIndexBuffer(*objModel->GetIndexBuffer(), DXGI_FORMAT_R32_UINT, 0);
	// Set primitive topology
	pd3dImmediateContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	XMMATRIX mWorldViewProjection = m_WorldMatrix * m_modelViewCamera.GetViewMatrix() * m_modelViewCamera.GetProjMatrix();
	HRESULT hr;
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	V(pd3dImmediateContext->Map(m_pFreqUpdateBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource));
	auto pCB = reinterpret_cast<FreqUpdatedCB*>(mappedResource.pData);
	XMStoreFloat4x4(&pCB->mWorldViewProj, XMMatrixTranspose(mWorldViewProjection));
	pCB->vMeshColor = XMFLOAT4(1.0f, 0.0f, 1.0f, 1.0f);
	pd3dImmediateContext->Unmap(m_pFreqUpdateBuffer, 0);

	// Render the cube
	pd3dImmediateContext->VSSetShader(m_pModelVS, nullptr, 0);
	pd3dImmediateContext->VSSetConstantBuffers(0, 1, &m_pFreqUpdateBuffer);
	pd3dImmediateContext->PSSetShader(m_pModelPS, nullptr, 0);
	pd3dImmediateContext->PSSetConstantBuffers(0, 1, &m_pFreqUpdateBuffer);
	pd3dImmediateContext->PSSetSamplers(0, 1, &m_pSamplerLinear);
	pd3dImmediateContext->DrawIndexed(objModel->GetIndicesCount(), 0, 0);

	ID3D11VertexShader* tmpVertexShader = nullptr;
	ID3D11PixelShader*  tmpPixelShader = nullptr;
	ID3D11Buffer*		tmpBuffer = nullptr;
	ID3D11ShaderResourceView* tmpSRV = nullptr;
	ID3D11SamplerState* tmpSampler = nullptr;
	pd3dImmediateContext->VSSetShader(tmpVertexShader, nullptr, 0);
	pd3dImmediateContext->VSSetConstantBuffers(0, 1, &tmpBuffer);
	pd3dImmediateContext->PSSetShader(tmpPixelShader, nullptr, 0);
	pd3dImmediateContext->PSSetConstantBuffers(0, 1, &tmpBuffer);
	pd3dImmediateContext->PSSetShaderResources(0, 1, &tmpSRV);
	pd3dImmediateContext->PSSetSamplers(0, 1, &tmpSampler);

	// Restore the blend state
	pd3dImmediateContext->OMSetBlendState(0, 0, 0xffffffff);
	pd3dImmediateContext->RSSetState(rs);
	SAFE_RELEASE(rs);
}

void VoxelViewer::UpdateCamera(float timeElapsed)
{
	m_modelViewCamera.FrameMove(timeElapsed);
}