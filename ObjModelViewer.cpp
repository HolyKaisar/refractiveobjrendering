#include "DXUT.h"
#include <cassert>
#include "ObjModelViewer.h"
#include "OBJ_Loader.h"

ObjModelViewer::ObjModelViewer()
{
	m_pVertexBuffer = nullptr;
	m_pIndexBuffer = nullptr;
	m_pInputLayout = nullptr;

	m_vertexStride = 0;
	m_vertexOffset = 0;
	m_indicesCount = 0;
	m_vertexCount  = 0;

	m_AABBMin = XMFLOAT3(INT_MAX, INT_MAX, INT_MAX);
	m_AABBMax = XMFLOAT3(INT_MIN, INT_MIN, INT_MIN);
}


ObjModelViewer::~ObjModelViewer()
{
	Destroy();
}

void ObjModelViewer::Destroy()
{
	SAFE_RELEASE(m_pVertexBuffer);
	SAFE_RELEASE(m_pIndexBuffer);
	SAFE_RELEASE(m_pInputLayout);

	m_vertexStride = 0;
	m_vertexOffset = 0;
	m_indicesCount = 0;
	m_vertexCount = 0;
}

HRESULT ObjModelViewer::LoadObjModel(ID3D11Device* pd3dDevice, string model_file_path)
{
	HRESULT hr = S_OK;
	objl::Loader m_loader;
	bool loadRes = m_loader.LoadFile(model_file_path);
	//assert(loadRes == false);
	if (!loadRes) return false;

	vector<BasicVertex> vertices;
	vector<uint32_t> indices;

	// Assuming each obj has one model
	//assert(m_loader.LoadedMeshes.size() != 1);
	objl::Mesh curMesh = m_loader.LoadedMeshes[0];

	m_AABBMin = XMFLOAT3(INT_MAX, INT_MAX, INT_MAX);
	m_AABBMax = XMFLOAT3(INT_MIN, INT_MIN, INT_MIN);
	m_vertexCount = curMesh.Vertices.size();
	for(auto vex : curMesh.Vertices)
	{
		BasicVertex bv;
		bv.mPos = XMFLOAT3(vex.Position.X, vex.Position.Y, vex.Position.Z);
		bv.mNormal = XMFLOAT3(vex.Normal.X, vex.Normal.Y, vex.Normal.Z);
		bv.mTex = XMFLOAT2(vex.TextureCoordinate.X, vex.TextureCoordinate.Y);
		vertices.push_back(bv);

		m_AABBMin.x = min(m_AABBMin.x, bv.mPos.x);
		m_AABBMin.y = min(m_AABBMin.y, bv.mPos.y);
		m_AABBMin.z = min(m_AABBMin.z, bv.mPos.z);
		m_AABBMax.x = max(m_AABBMax.x, bv.mPos.x);
		m_AABBMax.y = max(m_AABBMax.y, bv.mPos.y);
		m_AABBMax.z = max(m_AABBMax.z, bv.mPos.z);
	}

	float cX = (m_AABBMax.x + m_AABBMin.x) / 2.0f;
	float cY = (m_AABBMax.y + m_AABBMin.y) / 2.0f;
	float cZ = (m_AABBMax.z + m_AABBMin.z) / 2.0f;
	// Move Object Bounding Box Center to the world coordinate origin 
	for (auto& v : vertices)
	{
		v.mPos.x -= cX;
		v.mPos.y -= cY;
		v.mPos.z -= cZ;
	}
	m_AABBMax.x -= cX, m_AABBMax.y -= cY, m_AABBMax.z -= cZ;
	m_AABBMin.x -= cX, m_AABBMin.y -= cY, m_AABBMin.z -= cZ;

	m_indicesCount = curMesh.Indices.size();
	for (int i = 0; i < m_indicesCount; i += 3)
	{
		indices.push_back(curMesh.Indices[i]);
		indices.push_back(curMesh.Indices[i + 1]);
		indices.push_back(curMesh.Indices[i + 2]);
	}

	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(BasicVertex) * m_vertexCount;
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = 0;
	D3D11_SUBRESOURCE_DATA InitData;
	ZeroMemory(&InitData, sizeof(InitData));
	InitData.pSysMem = vertices.data();
	V_RETURN(pd3dDevice->CreateBuffer(&bd, &InitData, &m_pVertexBuffer));

	m_vertexStride = sizeof(BasicVertex);
	m_vertexOffset = 0;

	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(DWORD) * m_indicesCount;
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bd.CPUAccessFlags = 0;
	bd.MiscFlags = 0;
	InitData.pSysMem = indices.data();
	V_RETURN(pd3dDevice->CreateBuffer(&bd, &InitData, &m_pIndexBuffer));

	// TODO Load Materia Information
	return hr;
}