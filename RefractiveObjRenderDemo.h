#pragma once
#include "GHeader.h"
#include "ObjModelViewer.h"
#include "Voxelizer.h"
#include "VoxelViewer.h"

class RefractiveObjRenderDemo
{
public:
	RefractiveObjRenderDemo();
	~RefractiveObjRenderDemo();

	// DX11 Resources
	HRESULT CreateResource(ID3D11Device* pd3dDevice, uint32_t bbWidth, uint32_t bbHeight);
	void    DestroyResource();
	void	Render(ID3D11DeviceContext* pd3dImmediateContext, ID3D11ShaderResourceView* inputSRV);
	void	UpdateCamera(float timeElapsed);

	void	BeginVoxelization(ID3D11DeviceContext* pd3dImmediateContext);
	void	VisulizeVoxels(ID3D11DeviceContext* pd3dImmediateContext);

	void	RenderGUI();

	LRESULT HandleMessages(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
	{
		m_modelViewCamera.HandleMessages(hWnd, uMsg, wParam, lParam);

		m_objVoxelViewer.HandleMessages(hWnd, uMsg, wParam, lParam);
		return 0;
	}

	ID3D11ShaderResourceView** GetRenderResultSRV() { return &m_pSRV; }
	// Test Area
	ID3D11ShaderResourceView** GetVoxelViewerResultSRV() { return m_objVoxelViewer.GetResultSRV(); }

private:
	CModelViewerCamera			m_modelViewCamera;

	ComPtr<ID3D11VertexShader> m_pVertexShader;
	ComPtr<ID3D11PixelShader> m_pPixelShader;
	ComPtr<ID3D11InputLayout> m_pVertexLayout;
	ComPtr<ID3D11SamplerState> m_pSamplerLinear;

	ComPtr<ID3D11Buffer> m_pVertexBuffer;
	ComPtr<ID3D11Buffer> m_pIndexBuffer;
	ComPtr<ID3D11Buffer> m_pFreqUpdateBuffer;

	ComPtr<ID3D11Texture2D> m_pTex2D;
	ComPtr<ID3D11ShaderResourceView> m_pSRV;
	ComPtr<ID3D11RenderTargetView> m_pRTV;

	ComPtr<ID3D11Texture2D> m_pDepthTex2D;
	ComPtr<ID3D11DepthStencilState> m_pDepthStencilState;
	ComPtr<ID3D11DepthStencilView> m_pDepthStencilView;

	XMMATRIX					m_WorldMatrix;

	D3D11_VIEWPORT				m_demoViewPort;

	uint32_t					m_bbWidth;
	uint32_t					m_bbHeight;

	ComPtr<ID3D11Device> m_pGlobalDevice;
	ComPtr<ID3D11DeviceContext> m_pGlobalImmediateContext;

	bool						m_bPerformVoxelization;

private:
	ObjModelViewer	m_objModelViewer;
	Voxelizer		m_objVoxelizer;
	VoxelViewer		m_objVoxelViewer;

private:
	struct BasicVertex
	{
		XMFLOAT3 mPos;
		XMFLOAT2 mTex;
	};

	struct FreqUpdatedCB
	{
		XMFLOAT4X4 mWorldViewProj;
		XMFLOAT4X4 mWorld;
		XMFLOAT4 vMeshColor;
	};
};

