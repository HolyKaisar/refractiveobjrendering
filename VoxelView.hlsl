
//--------------------------------------------------------------------------------------
// Constant Buffer Variables
//--------------------------------------------------------------------------------------
Texture3D<float4> VolumeData : register(t0);
SamplerState samLinear : register(s0);

cbuffer cbChangesEveryFrame : register(b0)
{
	matrix WorldViewProj;
	uint4  vVolumeDimension;
	float4 vVolumeDimensionScales;
	float4 vMeshColor;
};

//--------------------------------------------------------------------------------------
struct VS_INPUT
{
	uint index : SV_VertexID;	// Use index to access voxel data.
};

struct GS_INPUT
{
	float3 Pos : POSITION;
	float3 Nor : TEXCOORD0;
};

struct PS_INPUT
{
	float4 Pos : SV_POSITION;
	float3 Nor : NORMAL;
	float2 Tex : TEXTURE;
};

struct VS_INPUT_MODEL
{
	float3 Pos : POSITION;
	float3 Nor : NORMAL;
	float2 Tex : TEXCOORD;
};

static const float3 boxOffset[24] =
{
	1, -1, 1,
	1, 1, 1,
	-1, -1, 1,
	-1, 1, 1,

	-1, 1, 1,
	-1, 1, -1,
	-1, -1, 1,
	-1, -1, -1,

	1, 1, -1,
	1, 1, 1,
	1, -1, -1,
	1, -1, 1,

	-1, 1, -1,
	1, 1, -1,
	-1, -1, -1,
	1, -1, -1,

	1, 1, 1,
	1, 1, -1,
	-1, 1, 1,
	-1, 1, -1,

	-1, -1, -1,
	1, -1, -1,
	-1, -1, 1,
	1, -1, 1
};

static const float2 boxTexArray[4] =
{
	0, 0,
	0, 1,
	1, 0,
	1, 1
};

static const float3 boxNormalArray[6] =
{
	0, 0, 1,
	-1, 0, 0,
	1, 0, 0,
	-1, 0, 0,
	0, 1, 0,
	0, -1, 0
};

//--------------------------------------------------------------------------------------
// Voxel Rendering Vertex Shader
//--------------------------------------------------------------------------------------
GS_INPUT VS(VS_INPUT input)
{
	GS_INPUT output = (GS_INPUT)0;
	uint w = vVolumeDimension.x;
	uint h = vVolumeDimension.y;
	uint d = vVolumeDimension.z;

	uint3 pos = uint3(0, 0, 0);
	uint sliceNum = w * w;
	pos.z = input.index / sliceNum;
	uint tmp = input.index % sliceNum;
	pos.y = tmp / w;
	pos.x = tmp % w;

	output.Pos = pos;
	uint scale = VolumeData.Load(uint4(pos, 0)).x;
	// TODO: need to store this information in the volume
	output.Nor = float3(scale, scale, scale);

	return output;
}

//--------------------------------------------------------------------------------------
// Model Rendering Vertex Shader
//--------------------------------------------------------------------------------------
PS_INPUT ModelVS(VS_INPUT_MODEL input)
{
	PS_INPUT output = (PS_INPUT)0;
	output.Pos = mul(float4(input.Pos, 1.0f), WorldViewProj);
	output.Nor = input.Nor;
	output.Tex = input.Tex;
	return output;
}


//--------------------------------------------------------------------------------------
// Geometry Shader
//--------------------------------------------------------------------------------------
[maxvertexcount(24)]
void GS(point GS_INPUT input[1], inout TriangleStream<PS_INPUT> triStream)
{
	// Only process vertexes with voxel data.
	if (input[0].Nor.x)
	{
		// Generate vertexes for six faces.
		for (int i = 0; i < 6; i++)
		{
			// Generate four vertexes for a face.
			for (int j = 0; j < 4; j++)
			{
				PS_INPUT outGS;
				// Create cube vertexes with boxOffset array and align the volume to the origin of world frame.
				float3 vertex = input[0].Pos.xyz + boxOffset[i * 4 + j] * 0.5f + float3(0.5f, 0.5f, 0.5f) - 0.5f * vVolumeDimension.xyz;
				
				// Output both geometry data for rendering a cube and voxel data for visualization.
				outGS.Pos = mul(float4(vertex * vVolumeDimensionScales.xyz, 1), WorldViewProj);
				outGS.Nor = boxNormalArray[i];
				outGS.Tex = boxTexArray[j];

				triStream.Append(outGS);

			}
			triStream.RestartStrip();
		}
	}

}


//--------------------------------------------------------------------------------------
// Voxel Rendering Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS(PS_INPUT input) : SV_Target
{
	return float4(1.0f, 0.0f, 1.0f, 1.0f);
}

//--------------------------------------------------------------------------------------
// Model Rendering Pixel Shader
//--------------------------------------------------------------------------------------
float4 ModelPS(PS_INPUT input) : SV_Target
{
	float3 lightPos = float3(2.0f, 5.0f, 2.0f);
	float3 lightDir = float3(0.0f, 0.0f, 0.0f) - lightPos;
	float3 lightD = -lightDir;
	lightD = normalize(lightD);

	float lightIntensity = saturate(dot(normalize(input.Nor), lightD));

	float4 color = float4(0.0f, 0.0f, 0.0f, 1.0f);
	float4 diffuseColor = float4(0.5f, 0.3f, 0.6f, 1.0f);
	if (lightIntensity > 0.0f)
	{
		// Determine the final diffuse color based on the diffuse color and the amount of light intensity.
		color += (diffuseColor * lightIntensity);
	}

	// TODO may need to add some lighting here; Phone Model First then a Unreal BRDF implementation
	return color;
}
