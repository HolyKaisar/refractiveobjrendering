#pragma once

#include <iostream>
#include <fstream>

using namespace std;
using namespace DirectX;

class ObjModelViewer
{
public:
	ObjModelViewer();
	~ObjModelViewer();

	HRESULT LoadObjModel(ID3D11Device* pd3dDevice, string model_file_path);
	void Destroy();

	uint32_t GetVertexStride() const { return m_vertexStride; }
	uint32_t GetVertexOffset() const { return m_vertexOffset; }
	uint32_t GetIndicesCount() const { return m_indicesCount; }
	uint32_t GetVertexCount()  const { return m_vertexCount; }
	//uint32_t GetVertexByteWidth() const { return m_vertexByteWidth; }
	//uint32_t GetIndexByteWidth() const { return m_indiexByteWidth; }

	ID3D11Buffer** GetVertexBuffer() { return &m_pVertexBuffer; }
	ID3D11Buffer** GetIndexBuffer() { return &m_pIndexBuffer; }

	XMFLOAT3 GetAABBMin() const { return m_AABBMin; }
	XMFLOAT3 GetAABBMax() const { return m_AABBMax; }

private:
	ID3D11Buffer*		m_pVertexBuffer;
	ID3D11Buffer*		m_pIndexBuffer;
	ID3D11InputLayout*	m_pInputLayout;

	uint32_t			m_vertexStride;
	uint32_t			m_vertexOffset;
	uint32_t			m_indicesCount;
	uint32_t			m_vertexCount;

	XMFLOAT3			m_AABBMin;
	XMFLOAT3			m_AABBMax;

private:
	struct BasicVertex
	{
		XMFLOAT3 mPos;
		XMFLOAT3 mNormal;
		XMFLOAT2 mTex;
	};
};

