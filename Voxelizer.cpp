#include "DXUT.h"
#include "Voxelizer.h"

Voxelizer::Voxelizer()
{
	m_pVertexShader = nullptr;
	m_pGeometryShader = nullptr;
	m_pPixelShader = nullptr;
	m_pVoxelizationLayout = nullptr;
	m_pFreqUpdateBuffer = nullptr;
	m_pSamplerLinear = nullptr;

	m_pVolumeTex = nullptr;
	m_pVolumeSRV = nullptr;
	m_pVolumeUAV = nullptr;

	m_pRTVTex2D = nullptr;
	m_pRTV = nullptr;

	m_pDepthTex2D = nullptr;
	m_pDepthStencilState = nullptr;
	m_pDepthStencilView = nullptr;

	m_pNoBackCullRS = nullptr;

	m_volumeWidth = 16;
	m_volumeHeight = 16;
	m_volumeDepth = 16;

	m_MSAASampleCount = 1;
	m_MSAASampleQuality = 0.0f;
}

Voxelizer::~Voxelizer()
{
	DestroyResource();
}

HRESULT Voxelizer::CreateResource(ID3D11Device* pd3dDevice, uint32_t bbWidth, uint32_t bbHeight)
{
	HRESULT hr = S_OK;

	DWORD dwShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;

#ifdef _DEBUG
	// Set the D3DCOMPILE_DEBUG flag to embed debug information in the shaders.
	// Setting this flag improves the shader debugging experience, but still allows 
	// the shaders to be optimized and to run exactly the way they will run in 
	// the release configuration of this program.
	dwShaderFlags |= D3DCOMPILE_DEBUG;

	// Disable optimizations to further improve shader debugging
	dwShaderFlags |= D3DCOMPILE_SKIP_OPTIMIZATION;
#endif

	// Compile the vertex shader
	ID3DBlob* pVSBlob = nullptr;
	V_RETURN(DXUTCompileFromFile(L"Voxelization.hlsl", nullptr, "VS", "vs_5_0", dwShaderFlags, 0, &pVSBlob));

	// Create the vertex shader
	hr = pd3dDevice->CreateVertexShader(pVSBlob->GetBufferPointer(), pVSBlob->GetBufferSize(), nullptr, &m_pVertexShader);
	if (FAILED(hr))
	{
		SAFE_RELEASE(pVSBlob);
		return hr;
	}

	// Define the input layout
	D3D11_INPUT_ELEMENT_DESC layout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT , D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT , D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	UINT numElements = ARRAYSIZE(layout);

	// Create the input layout
	hr = pd3dDevice->CreateInputLayout(layout, numElements, pVSBlob->GetBufferPointer(),
		pVSBlob->GetBufferSize(), &m_pVoxelizationLayout);
	SAFE_RELEASE(pVSBlob);
	if (FAILED(hr))
		return hr;

	// Compile the geometry shader
	ID3DBlob* pGSBlob = nullptr;
	V_RETURN(DXUTCompileFromFile(L"Voxelization.hlsl", nullptr, "GS", "gs_5_0", dwShaderFlags, 0, &pGSBlob));

	// Create the geometry shader
	hr = pd3dDevice->CreateGeometryShader(pGSBlob->GetBufferPointer(), pGSBlob->GetBufferSize(), nullptr, &m_pGeometryShader);
	SAFE_RELEASE(pGSBlob);
	if (FAILED(hr))
		return hr;

	// Compile the pixel shader
	ID3DBlob* pPSBlob = nullptr;
	V_RETURN(DXUTCompileFromFile(L"Voxelization.hlsl", nullptr, "PS", "ps_5_0", dwShaderFlags, 0, &pPSBlob));

	// Create the pixel shader
	hr = pd3dDevice->CreatePixelShader(pPSBlob->GetBufferPointer(), pPSBlob->GetBufferSize(), nullptr, &m_pPixelShader);
	SAFE_RELEASE(pPSBlob);
	if (FAILED(hr))
		return hr;

	// Create the constant buffers
	D3D11_BUFFER_DESC bd;
	bd.Usage = D3D11_USAGE_DYNAMIC;
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	bd.ByteWidth = sizeof(FreqUpdatedCB);
	bd.MiscFlags = 0;
	V_RETURN(pd3dDevice->CreateBuffer(&bd, nullptr, &m_pFreqUpdateBuffer));

	// Create the sample state
	D3D11_SAMPLER_DESC sampDesc;
	ZeroMemory(&sampDesc, sizeof(sampDesc));
	sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	sampDesc.MinLOD = 0;
	sampDesc.MaxLOD = D3D11_FLOAT32_MAX;
	V_RETURN(pd3dDevice->CreateSamplerState(&sampDesc, &m_pSamplerLinear));

	m_voxelViewPort.TopLeftX = 0;
	m_voxelViewPort.TopLeftY = 0;
	m_voxelViewPort.MinDepth = 0.0f;
	m_voxelViewPort.MaxDepth = 1.0f;
	m_voxelViewPort.Width = m_volumeWidth;
	m_voxelViewPort.Height = m_volumeHeight;

	// Create No Back Culling RS;
	D3D11_RASTERIZER_DESC rasterizerState;
	rasterizerState.FillMode = D3D11_FILL_SOLID;
	rasterizerState.CullMode = D3D11_CULL_NONE;
	rasterizerState.FrontCounterClockwise = true;
	rasterizerState.DepthBias = false;
	rasterizerState.DepthBiasClamp = 0;
	rasterizerState.SlopeScaledDepthBias = 0;
	rasterizerState.DepthClipEnable = false;
	rasterizerState.ScissorEnable = false;
	rasterizerState.MultisampleEnable = true;
	rasterizerState.AntialiasedLineEnable = false;
	V_RETURN(pd3dDevice->CreateRasterizerState(&rasterizerState, &m_pNoBackCullRS));

	CreateRenderTargetResource(pd3dDevice);

	Create3DVolumeResource(pd3dDevice);
	return hr;
}

void Voxelizer::DestroyResource()
{
	SAFE_RELEASE(m_pVertexShader);
	SAFE_RELEASE(m_pGeometryShader);
	SAFE_RELEASE(m_pPixelShader);
	SAFE_RELEASE(m_pVoxelizationLayout);
	SAFE_RELEASE(m_pFreqUpdateBuffer);
	SAFE_RELEASE(m_pSamplerLinear);

	SAFE_RELEASE(m_pVolumeTex);
	SAFE_RELEASE(m_pVolumeSRV);
	SAFE_RELEASE(m_pVolumeUAV);

	SAFE_RELEASE(m_pRTVTex2D);
	SAFE_RELEASE(m_pRTV);

	SAFE_RELEASE(m_pDepthTex2D);
	SAFE_RELEASE(m_pDepthStencilState);
	SAFE_RELEASE(m_pDepthStencilView);

	SAFE_RELEASE(m_pNoBackCullRS);
}

void Voxelizer::Voxelization(ID3D11DeviceContext* pd3dImmediateContext, ObjModelViewer* objModel)
{
	ID3D11RasterizerState* rs = nullptr;
	pd3dImmediateContext->RSGetState(&rs);

	// Clear all voxel data to zero.
	const float zero[4] = { 0, 0, 0, 0 };
	pd3dImmediateContext->ClearUnorderedAccessViewFloat(m_pVolumeUAV, zero);
	pd3dImmediateContext->OMSetRenderTargetsAndUnorderedAccessViews(1, &m_pRTV, nullptr, 1, 1, &m_pVolumeUAV, nullptr);
	// Save all voxel data to an UAV, so it doesn't use any render target.
	pd3dImmediateContext->OMSetDepthStencilState(m_pDepthStencilState, 1);

	pd3dImmediateContext->RSSetViewports(1, &m_voxelViewPort);
	// Set the input layout
	pd3dImmediateContext->IASetInputLayout(m_pVoxelizationLayout);
	// Set No Back Culling Rasterizer State
	pd3dImmediateContext->RSSetState(m_pNoBackCullRS);

	// Set vertex buffer/
	UINT stride = objModel->GetVertexStride();
	UINT offset = 0;
	pd3dImmediateContext->IASetVertexBuffers(0, 1, objModel->GetVertexBuffer(), &stride, &offset);
	// Set index buffer
	pd3dImmediateContext->IASetIndexBuffer(*objModel->GetIndexBuffer(), DXGI_FORMAT_R32_UINT, 0);
	// Set primitive topology
	pd3dImmediateContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	XMMATRIX mWorldViewProjection = XMMatrixIdentity();
	HRESULT hr;
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	V(pd3dImmediateContext->Map(m_pFreqUpdateBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource));
	auto pCB = reinterpret_cast<FreqUpdatedCB*>(mappedResource.pData);
	XMStoreFloat4x4(&pCB->mWorldViewProj, XMMatrixTranspose(mWorldViewProjection));
	XMFLOAT3 minPt = objModel->GetAABBMin(), maxPt = objModel->GetAABBMax();
	pCB->vDimScales = XMFLOAT4(2.0f / (maxPt.x - minPt.x), 2.0f / (maxPt.y - minPt.y), 2.0f / (maxPt.z - minPt.z), 1.0f); // TODO may need to prevent denominator to be zero
	pCB->vVolumeScales = XMFLOAT4((maxPt.x - minPt.x) / m_volumeWidth, (maxPt.y - minPt.y) / m_volumeHeight, (maxPt.z - minPt.z) / m_volumeDepth, 1.0f); // TODO may need to prevent denominator to be zero
	pCB->vMeshColor = XMFLOAT4(1.0f, 0.0f, 1.0f, 1.0f);
	pd3dImmediateContext->Unmap(m_pFreqUpdateBuffer, 0);

	// Render the cube
	pd3dImmediateContext->VSSetShader(m_pVertexShader, nullptr, 0);
	pd3dImmediateContext->VSSetConstantBuffers(0, 1, &m_pFreqUpdateBuffer);
	pd3dImmediateContext->GSSetShader(m_pGeometryShader, nullptr, 0);
	pd3dImmediateContext->PSSetShader(m_pPixelShader, nullptr, 0);
	pd3dImmediateContext->PSSetConstantBuffers(0, 1, &m_pFreqUpdateBuffer);
	//pd3dImmediateContext->PSSetShaderResources(0, 1, &m_pVoxelizationLayout);
	pd3dImmediateContext->PSSetSamplers(0, 1, &m_pSamplerLinear);
	pd3dImmediateContext->DrawIndexed(objModel->GetIndicesCount(), 0, 0);

	ID3D11VertexShader* tmpVertexShader = nullptr;
	ID3D11PixelShader* tmpPixelShader = nullptr;
	ID3D11GeometryShader* tmpGeometryShader = nullptr;
	ID3D11Buffer* tmpBuffer = nullptr;
	ID3D11ShaderResourceView* tmpSRV = nullptr;
	ID3D11SamplerState* tmpSampler = nullptr;
	pd3dImmediateContext->VSSetShader(tmpVertexShader, nullptr, 0);
	pd3dImmediateContext->VSSetConstantBuffers(0, 1, &tmpBuffer);
	pd3dImmediateContext->GSSetShader(tmpGeometryShader, nullptr, 0);
	pd3dImmediateContext->PSSetShader(tmpPixelShader, nullptr, 0);
	pd3dImmediateContext->PSSetConstantBuffers(0, 1, &tmpBuffer);
	pd3dImmediateContext->PSSetShaderResources(0, 1, &tmpSRV);
	pd3dImmediateContext->PSSetSamplers(0, 1, &tmpSampler);

	// Restore the blend state
	ID3D11UnorderedAccessView* ppNullUAV = nullptr;
	pd3dImmediateContext->OMSetRenderTargetsAndUnorderedAccessViews(0, nullptr, nullptr, 0, 1, &ppNullUAV, nullptr);
	pd3dImmediateContext->OMSetBlendState(0, 0, 0xffffffff);
	pd3dImmediateContext->RSSetState(rs);

	SAFE_RELEASE(rs)
}

void Voxelizer::UpdateVolumeDimensions(uint32_t _width, uint32_t _height, uint32_t _depth)
{
	m_volumeWidth = _width;
	m_volumeHeight = _height;
	m_volumeDepth = _depth;
}

void Voxelizer::SetMSAASampleCount(const int msaaSampleCount)
{
	m_MSAASampleCount = msaaSampleCount;
}

void Voxelizer::SetMSAASampleQuality(const float msaaSampleQuality)
{
	m_MSAASampleQuality = msaaSampleQuality;
}

/***************************************/
/*    Private Function Definitions     */
/***************************************/
HRESULT Voxelizer::Create3DVolumeResource(ID3D11Device* pd3dDevice) // TODO format should be a parameter of 
{
	HRESULT hr = S_OK;

	D3D11_TEXTURE3D_DESC dstex;
	dstex.Width = m_volumeWidth;
	dstex.Height = m_volumeHeight;
	dstex.Depth = m_volumeDepth;
	dstex.MipLevels = 0;
	//dstex.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	dstex.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	//dstex.Format = DXGI_FORMAT_R32_FLOAT;
	dstex.Usage = D3D11_USAGE_DEFAULT;
	dstex.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_UNORDERED_ACCESS;
	dstex.CPUAccessFlags = 0;
	dstex.MiscFlags = 0;

	V_RETURN(pd3dDevice->CreateTexture3D(&dstex, nullptr, &m_pVolumeTex));

	// Create the render target views.
	D3D11_UNORDERED_ACCESS_VIEW_DESC  UAVDesc;
	UAVDesc.Format = dstex.Format;
	UAVDesc.ViewDimension = D3D11_UAV_DIMENSION_TEXTURE3D;
	UAVDesc.Texture3D.FirstWSlice = 0;
	UAVDesc.Texture3D.MipSlice = 0;
	UAVDesc.Texture3D.WSize = m_volumeDepth;
	V_RETURN(pd3dDevice->CreateUnorderedAccessView(m_pVolumeTex, &UAVDesc, &m_pVolumeUAV));

	// Create the resource view.
	D3D11_SHADER_RESOURCE_VIEW_DESC SRVDesc;
	ZeroMemory(&SRVDesc, sizeof(SRVDesc));
	SRVDesc.Format = dstex.Format;
	SRVDesc.Texture3D.MipLevels = 1;
	SRVDesc.Texture3D.MostDetailedMip = 0;
	SRVDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE3D;

	V_RETURN(pd3dDevice->CreateShaderResourceView(m_pVolumeTex, &SRVDesc, &m_pVolumeSRV));
	return hr;
}

HRESULT Voxelizer::CreateRenderTargetResource(ID3D11Device* pd3dDevice)
{
	HRESULT hr = S_OK;

	SAFE_RELEASE(m_pDepthTex2D);
	SAFE_RELEASE(m_pDepthStencilState);
	SAFE_RELEASE(m_pDepthStencilView);
	SAFE_RELEASE(m_pRTVTex2D);
	SAFE_RELEASE(m_pRTV);

	// Create Stencil View
	D3D11_TEXTURE2D_DESC descDepth;
	ZeroMemory(&descDepth, sizeof(descDepth));
	descDepth.Width = m_volumeWidth;
	descDepth.Height = m_volumeHeight;
	descDepth.MipLevels = 1;
	descDepth.ArraySize = 1;
	descDepth.Format = DXUTGetDeviceSettings().d3d11.AutoDepthStencilFormat;
	//descDepth.SampleDesc.Count = 1;
	//descDepth.SampleDesc.Quality = 0;
	descDepth.SampleDesc.Count = m_MSAASampleCount;
	descDepth.SampleDesc.Quality = m_MSAASampleQuality;
	descDepth.Usage = D3D11_USAGE_DEFAULT;
	descDepth.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	descDepth.CPUAccessFlags = 0;
	descDepth.MiscFlags = 0;
	V_RETURN(pd3dDevice->CreateTexture2D(&descDepth, NULL, &m_pDepthTex2D));

	D3D11_DEPTH_STENCIL_VIEW_DESC descDSV;
	ZeroMemory(&descDSV, sizeof(descDSV));
	descDSV.Format = descDepth.Format;
	//descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS;
	descDSV.Texture2D.MipSlice = 0;

	// Create Depth Stencil State
	D3D11_DEPTH_STENCIL_DESC dsDesc;
	// Depth test parameters
	dsDesc.DepthEnable = false; // Disable Depth Test
	dsDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	dsDesc.DepthFunc = D3D11_COMPARISON_ALWAYS;
	// Stencil test parameters
	dsDesc.StencilEnable = true;
	dsDesc.StencilReadMask = 0xFF;
	dsDesc.StencilWriteMask = 0xFF;
	// Stencil operations if pixel is front-facing
	dsDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	dsDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	// Stencil operations if pixel is back-facing
	dsDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	dsDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	V_RETURN(pd3dDevice->CreateDepthStencilState(&dsDesc, &m_pDepthStencilState));

	// Create the depth stencil view
	V_RETURN(pd3dDevice->CreateDepthStencilView(m_pDepthTex2D, // Depth stencil texture
		&descDSV, // Depth stencil desc
		&m_pDepthStencilView));

	// Create Render Target
	D3D11_TEXTURE2D_DESC	RTtextureDesc = { 0 };
	RTtextureDesc.Width = m_volumeWidth;
	RTtextureDesc.Height = m_volumeHeight;
	RTtextureDesc.MipLevels = 1;
	RTtextureDesc.ArraySize = 1;
	//RTtextureDesc.Format = DXGI_FORMAT_R32_FLOAT;
	RTtextureDesc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	//RTtextureDesc.SampleDesc.Count = 1;
	RTtextureDesc.SampleDesc.Count = m_MSAASampleCount;
	RTtextureDesc.SampleDesc.Quality = m_MSAASampleQuality;
	RTtextureDesc.Usage = D3D11_USAGE_DEFAULT;
	RTtextureDesc.BindFlags = D3D11_BIND_RENDER_TARGET;
	RTtextureDesc.CPUAccessFlags = 0;
	RTtextureDesc.MiscFlags = 0;
	V_RETURN(pd3dDevice->CreateTexture2D(&RTtextureDesc, NULL, &m_pRTVTex2D));

	D3D11_RENDER_TARGET_VIEW_DESC	RTviewDesc;
	RTviewDesc.Format = RTtextureDesc.Format;
	//RTviewDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
	RTviewDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2DMS;
	RTviewDesc.Texture2D.MipSlice = 0;
	V_RETURN(pd3dDevice->CreateRenderTargetView(m_pRTVTex2D, &RTviewDesc, &m_pRTV));

	return hr;
}