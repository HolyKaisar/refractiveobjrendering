#pragma once

#include "GHeader.h"

class BGSquare
{
public:
	BGSquare();
	~BGSquare();

	HRESULT CreateResource(ID3D11Device* pd3dDevice, uint32_t bbWidth, uint32_t bbHeight);
	void    DestroyResource();

	void	Render(ID3D11DeviceContext* pd3dImmediateContext, ID3D11ShaderResourceView** inputSRV);

private:
	ID3D11VertexShader*			m_pVertexShader;
	ID3D11PixelShader*			m_pPixelShader;
	ID3D11InputLayout*			m_pVertexLayout;
	ID3D11SamplerState*			m_pSamplerLinear;

	ID3D11Buffer*				m_pVertexBuffer;
	ID3D11Buffer*				m_pIndexBuffer;
	ID3D11Buffer*				m_pFreqUpdateBuffer;

	ID3D11Texture2D*			m_pTex2D;
	ID3D11ShaderResourceView*	m_pSRV;
	ID3D11RenderTargetView*		m_pRTV;

	XMMATRIX					m_WorldMatrix;
	XMMATRIX					m_ViewMatrix;
	XMMATRIX					m_ProjMatrix;

private:
	struct BasicVertex
	{
		XMFLOAT3 mPos;
		XMFLOAT2 mTex;
	};

	struct FreqUpdatedCB
	{
		XMFLOAT4X4 mWorldViewProj;
		XMFLOAT4X4 mWorld;
		XMFLOAT4 vMeshColor;
	};
};

