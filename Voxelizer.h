#pragma once
#include "GHeader.h"
#include "ObjModelViewer.h"

class Voxelizer
{
public:
	Voxelizer();
	~Voxelizer();

	HRESULT CreateResource(ID3D11Device* pd3dDevice, uint32_t bbWidth, uint32_t bbHeight);
	HRESULT CreateRenderTargetResource(ID3D11Device* pd3dDevice);

	void    DestroyResource();
	void	Voxelization(ID3D11DeviceContext* pd3dImmediateContext, ObjModelViewer* objModel);

	void	UpdateVolumeDimensions(uint32_t _width, uint32_t _height, uint32_t _depth);

	ID3D11ShaderResourceView** GetVolumeSRV() { return &m_pVolumeSRV; }

	uint32_t GetVolumeWidth() const { return m_volumeWidth; }
	uint32_t GetVolumeHeight() const { return m_volumeHeight; }
	uint32_t GetVolumeDepth() const { return m_volumeDepth; }

	void SetMSAASampleCount(const int msaaSampleCount);
	void SetMSAASampleQuality(const float msaaSampleQuality);

private:
	// Voxelization Resources
	ID3D11VertexShader* m_pVertexShader;
	ID3D11GeometryShader* m_pGeometryShader;
	ID3D11PixelShader* m_pPixelShader;
	ID3D11InputLayout* m_pVoxelizationLayout;
	ID3D11Buffer* m_pFreqUpdateBuffer;
	ID3D11SamplerState* m_pSamplerLinear;

	ID3D11Texture3D* m_pVolumeTex;
	ID3D11ShaderResourceView* m_pVolumeSRV;
	ID3D11UnorderedAccessView* m_pVolumeUAV;

	ID3D11Texture2D* m_pRTVTex2D;
	ID3D11RenderTargetView* m_pRTV;

	ID3D11Texture2D* m_pDepthTex2D;
	ID3D11DepthStencilState* m_pDepthStencilState;
	ID3D11DepthStencilView* m_pDepthStencilView;

	ID3D11RasterizerState* m_pNoBackCullRS;

	D3D11_VIEWPORT				m_voxelViewPort;

	uint32_t					m_volumeWidth;
	uint32_t					m_volumeHeight;
	uint32_t					m_volumeDepth;

	float						m_voxelWidth;
	float						m_voxelHeight;
	float						m_voxelDepth;

	uint32_t					m_MSAASampleCount;
	float						m_MSAASampleQuality;

private:
	HRESULT Create3DVolumeResource(ID3D11Device* pd3dDevice);

private:
	struct BasicVertex
	{
		XMFLOAT3 mPos;
		XMFLOAT3 mNor;
		XMFLOAT2 mTex;
	};

	struct FreqUpdatedCB
	{
		XMFLOAT4X4 mWorldViewProj;
		XMFLOAT4 vDimScales;
		XMFLOAT4 vVolumeScales;
		XMFLOAT4 vMeshColor;
		XMFLOAT4 padding;
	};
};

