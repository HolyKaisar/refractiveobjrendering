
//--------------------------------------------------------------------------------------
// Constant Buffer Variables
//--------------------------------------------------------------------------------------
SamplerState samLinear : register(s0);
//RWTexture3D<float4> uav : register(u1);
RWTexture3D<float4> uav : register(u1);

cbuffer cbChangesEveryFrame : register(b0)
{
	matrix WorldViewProj;
	float4 vDimScales;
	float4 vVolumeScales;
	float4 vMeshColor;
	float4 padding;
};

//--------------------------------------------------------------------------------------
struct VS_INPUT
{
	float3 Pos : POSITION;
	float3 Nor : NORMAL;
	float2 Tex : TEXCOORD;
};

struct GS_INPUT
{
	float4 Pos : POSITION;
	float3 Nor : TEXCOORD0;
	float2 Tex : TEXCOORD1;
};

struct PS_INPUT
{
	float4 Pos : SV_POSITION;
	float4 PosV : TEXCOORD0;
	float3 Nor : NORMAL;
	float2 Tex : TEXCOORD1;
};


//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
GS_INPUT VS(VS_INPUT input)
{
	GS_INPUT output = (GS_INPUT)0;
	output.Pos = float4(input.Pos, 1.0f) * vDimScales;
	output.Nor = input.Nor;
	output.Tex = input.Tex;

	return output;
}

//--------------------------------------------------------------------------------------
// Geometry Shader
//--------------------------------------------------------------------------------------
[maxvertexcount(12)]
void GS(triangle GS_INPUT input[3], inout TriangleStream<PS_INPUT> triStream)
{
	// Assuming view direction is the z negative, and the RH space
	PS_INPUT output1 = (PS_INPUT)0;
	PS_INPUT output2 = (PS_INPUT)0;
	PS_INPUT output3 = (PS_INPUT)0;
	float area = 0.0f, tArea = 0.0f;

	float3 edgeOne = float3(input[1].Pos.xy - input[0].Pos.xy, 0.0f);
	float3 edgeTwo = float3(input[2].Pos.xy - input[0].Pos.xy, 0.0f);
	tArea = 0.5f * abs(edgeOne.x * edgeTwo.y - edgeTwo.x * edgeOne.y);
	if (tArea > area)
	{
		output1.Pos = float4(input[0].Pos.xy, 1, 1);
		output2.Pos = float4(input[1].Pos.xy, 1, 1);
		output3.Pos = float4(input[2].Pos.xy, 1, 1);
		area = tArea;
	}

	edgeOne = float3(input[1].Pos.yz - input[0].Pos.yz, 0.0f);
	edgeTwo = float3(input[2].Pos.yz - input[0].Pos.yz, 0.0f);
	tArea = 0.5f * abs(edgeOne.x * edgeTwo.y - edgeTwo.x * edgeOne.y);
	if (tArea > area)
	{
		output1.Pos = float4(input[0].Pos.yz, 1.0f, 1.0f);
		output2.Pos = float4(input[1].Pos.yz, 1.0f, 1.0f);
		output3.Pos = float4(input[2].Pos.yz, 1.0f, 1.0f);
		area = tArea;
	}

	edgeOne = float3(input[1].Pos.xz - input[0].Pos.xz, 0.0f);
	edgeTwo = float3(input[2].Pos.xz - input[0].Pos.xz, 0.0f);
	tArea = 0.5f * abs(edgeOne.x * edgeTwo.y - edgeTwo.x * edgeOne.y);
	if (tArea > area)
	{
		output1.Pos = float4(input[0].Pos.xz, 1.0f, 1.0f);
		output2.Pos = float4(input[1].Pos.xz, 1.0f, 1.0f);
		output3.Pos = float4(input[2].Pos.xz, 1.0f, 1.0f);
		area = tArea;
	}

	output1.PosV = input[0].Pos, output1.Nor = input[0].Nor, output1.Tex = input[0].Tex;
	output2.PosV = input[1].Pos, output2.Nor = input[1].Nor, output2.Tex = input[1].Tex;
	output3.PosV = input[2].Pos, output3.Nor = input[2].Nor, output3.Tex = input[2].Tex;

	triStream.Append(output1);
	triStream.Append(output2);
	triStream.Append(output3);

	triStream.RestartStrip();
}


//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS(PS_INPUT input) : SV_Target
{
	// Hard code volumn dimension
	float4 	newPos = (input.PosV + float4(1.0f, 1.0f, 1.0f, 0.0f)) / vDimScales / vVolumeScales;
	float3  newNor = input.Nor;

	uav[uint3(newPos.xyz)] = float4(1.0f, 1.0f, 1.0f, 1.0f);

	return input.PosV;
}
