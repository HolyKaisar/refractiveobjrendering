#pragma once

#include "GHeader.h"
#include "ObjModelViewer.h"

class VoxelViewer
{
public:
	VoxelViewer();
	~VoxelViewer();

	HRESULT CreateResource(ID3D11Device* pd3dDevice, uint32_t bbWidth, uint32_t bbHeight);
	void    DestroyResource();
	void	RenderVoxels(ID3D11DeviceContext* pd3dImmediateContext, ID3D11ShaderResourceView** inputSRV, 
						uint32_t& vWidth, uint32_t& vHeight, uint32_t& vDepth, XMFLOAT4& volumeScales);
	void	RenderModel(ID3D11DeviceContext* pd3dImmediateContext, ObjModelViewer* objModel);
	void	UpdateCamera(float timeElapsed);

	//ID3D11ShaderResourceView** GetResultSRV() { return &m_pRTVSRV; }
	ID3D11ShaderResourceView** GetResultSRV() { return &m_pModelSRV; }

	LRESULT HandleMessages(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
	{
		m_modelViewCamera.HandleMessages(hWnd, uMsg, wParam, lParam);
		return 0;
	}

private:
	CModelViewerCamera			m_modelViewCamera;

	ID3D11VertexShader*			m_pVertexShader;
	ID3D11GeometryShader*		m_pGeometryShader;
	ID3D11PixelShader*			m_pPixelShader;
	ID3D11InputLayout*			m_pVoxelViewLayout;
	ID3D11Buffer*				m_pFreqUpdateBuffer;
	ID3D11SamplerState*			m_pSamplerLinear;

	ID3D11Texture2D*			m_pRTVTex2D;
	ID3D11ShaderResourceView*	m_pRTVSRV;
	ID3D11RenderTargetView*		m_pRTV;

	ID3D11Texture2D*			m_pDepthTex2D;
	ID3D11DepthStencilState*	m_pDepthStencilState;
	ID3D11DepthStencilView*		m_pDepthStencilView;
	D3D11_VIEWPORT				m_viewPort;

	ID3D11RasterizerState*      m_pWireFrameRS;
	ID3D11RasterizerState*      m_pSolidStateRS;

	XMMATRIX					m_WorldMatrix;


	// GPU Resources for Rendering Model
	ID3D11VertexShader*			m_pModelVS;
	ID3D11PixelShader*			m_pModelPS;
	ID3D11InputLayout*			m_pModelLayout;

	ID3D11Texture2D*			m_pModelTex2D;
	ID3D11ShaderResourceView*	m_pModelSRV;
	ID3D11RenderTargetView*		m_pModelRTV;

private:
	struct FreqUpdatedCB
	{
		XMFLOAT4X4 mWorldViewProj;
		XMUINT4	 vVolumeDimension;
		XMFLOAT4 vVolumeDimensionScale;
		XMFLOAT4 vMeshColor;
	};
};

