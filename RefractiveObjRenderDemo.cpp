#include "DXUT.h"
#include "RefractiveObjRenderDemo.h"


RefractiveObjRenderDemo::RefractiveObjRenderDemo()
{
	m_pVertexShader = nullptr;
	m_pPixelShader = nullptr;
	m_pVertexLayout = nullptr;
	m_pSamplerLinear = nullptr;

	m_pVertexBuffer = nullptr;
	m_pIndexBuffer = nullptr;
	m_pFreqUpdateBuffer = nullptr;

	m_pTex2D = nullptr;
	m_pSRV = nullptr;
	m_pRTV = nullptr;

	m_pDepthTex2D = nullptr;
	m_pDepthStencilState = nullptr;
	m_pDepthStencilView = nullptr;

	m_pGlobalDevice = nullptr;

	m_bbWidth = m_bbHeight = 1;

	m_bPerformVoxelization = true;
}

RefractiveObjRenderDemo::~RefractiveObjRenderDemo()
{
	DestroyResource();
}

void RefractiveObjRenderDemo::DestroyResource()
{
	m_pVertexShader.Reset();
	m_pPixelShader.Reset();
	m_pVertexLayout.Reset();
	m_pSamplerLinear.Reset();
	//SAFE_RELEASE(m_pPixelShader);
	//SAFE_RELEASE(m_pVertexLayout);
	//SAFE_RELEASE(m_pSamplerLinear);

	m_pVertexBuffer.Reset();
	m_pIndexBuffer.Reset();
	m_pFreqUpdateBuffer.Reset();
	//SAFE_RELEASE(m_pVertexBuffer);
	//SAFE_RELEASE(m_pIndexBuffer);
	//SAFE_RELEASE(m_pFreqUpdateBuffer);

	m_pTex2D.Reset();
	m_pSRV.Reset();
	m_pRTV.Reset();
	//SAFE_RELEASE(m_pTex2D);
	//SAFE_RELEASE(m_pSRV);
	//SAFE_RELEASE(m_pRTV);

	m_pDepthTex2D.Reset();
	m_pDepthStencilState.Reset();
	m_pDepthStencilView.Reset();
	//SAFE_RELEASE(m_pDepthTex2D);
	//SAFE_RELEASE(m_pDepthStencilState);
	//SAFE_RELEASE(m_pDepthStencilView);

	m_objModelViewer.Destroy();
	m_objVoxelizer.DestroyResource();
	m_objVoxelViewer.DestroyResource();

	//m_pGlobalDevice = nullptr;
	m_pGlobalDevice.Reset();
	m_pGlobalImmediateContext.Reset();
}

HRESULT RefractiveObjRenderDemo::CreateResource(ID3D11Device* pd3dDevice, uint32_t bbWidth, uint32_t bbHeight)
{
	HRESULT hr = S_OK;

	DWORD dwShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;

#ifdef _DEBUG
	// Set the D3DCOMPILE_DEBUG flag to embed debug information in the shaders.
	// Setting this flag improves the shader debugging experience, but still allows 
	// the shaders to be optimized and to run exactly the way they will run in 
	// the release configuration of this program.
	dwShaderFlags |= D3DCOMPILE_DEBUG;

	// Disable optimizations to further improve shader debugging
	dwShaderFlags |= D3DCOMPILE_SKIP_OPTIMIZATION;
#endif

	// Compile the vertex shader
	ID3DBlob* pVSBlob = nullptr;
	V_RETURN(DXUTCompileFromFile(L"RefractiveShader.hlsl", nullptr, "VS", "vs_5_0", dwShaderFlags, 0, &pVSBlob));

	// Create the vertex shader
	hr = pd3dDevice->CreateVertexShader(pVSBlob->GetBufferPointer(), pVSBlob->GetBufferSize(), nullptr, m_pVertexShader.GetAddressOf());
	//hr = pd3dDevice->CreateVertexShader(pVSBlob->GetBufferPointer(), pVSBlob->GetBufferSize(), nullptr, &m_pVertexShader);
	if (FAILED(hr))
	{
		SAFE_RELEASE(pVSBlob);
		return hr;
	}

	// Define the input layout
	D3D11_INPUT_ELEMENT_DESC layout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT , D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT , D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	UINT numElements = ARRAYSIZE(layout);

	// Create the input layout
	hr = pd3dDevice->CreateInputLayout(layout, numElements, pVSBlob->GetBufferPointer(),
		pVSBlob->GetBufferSize(), &m_pVertexLayout);
	SAFE_RELEASE(pVSBlob);
	if (FAILED(hr))
		return hr;


	// Compile the pixel shader
	ID3DBlob* pPSBlob = nullptr;
	V_RETURN(DXUTCompileFromFile(L"RefractiveShader.hlsl", nullptr, "PS", "ps_5_0", dwShaderFlags, 0, &pPSBlob));

	// Create the pixel shader
	hr = pd3dDevice->CreatePixelShader(pPSBlob->GetBufferPointer(), pPSBlob->GetBufferSize(), nullptr, &m_pPixelShader);
	SAFE_RELEASE(pPSBlob);
	if (FAILED(hr))
		return hr;

	// Create test cube vertex buffer
	BasicVertex vertices[] =
	{
		{ XMFLOAT3(-1.0f, 1.0f, -1.0f), XMFLOAT2(1.0f, 0.0f) },
		{ XMFLOAT3(1.0f, 1.0f, -1.0f), XMFLOAT2(0.0f, 0.0f) },
		{ XMFLOAT3(1.0f, 1.0f, 1.0f), XMFLOAT2(0.0f, 1.0f) },
		{ XMFLOAT3(-1.0f, 1.0f, 1.0f), XMFLOAT2(1.0f, 1.0f) },

		{ XMFLOAT3(-1.0f, -1.0f, -1.0f), XMFLOAT2(0.0f, 0.0f) },
		{ XMFLOAT3(1.0f, -1.0f, -1.0f), XMFLOAT2(1.0f, 0.0f) },
		{ XMFLOAT3(1.0f, -1.0f, 1.0f), XMFLOAT2(1.0f, 1.0f) },
		{ XMFLOAT3(-1.0f, -1.0f, 1.0f), XMFLOAT2(0.0f, 1.0f) },

		{ XMFLOAT3(-1.0f, -1.0f, 1.0f), XMFLOAT2(0.0f, 1.0f) },
		{ XMFLOAT3(-1.0f, -1.0f, -1.0f), XMFLOAT2(1.0f, 1.0f) },
		{ XMFLOAT3(-1.0f, 1.0f, -1.0f), XMFLOAT2(1.0f, 0.0f) },
		{ XMFLOAT3(-1.0f, 1.0f, 1.0f), XMFLOAT2(0.0f, 0.0f) },

		{ XMFLOAT3(1.0f, -1.0f, 1.0f), XMFLOAT2(1.0f, 1.0f) },
		{ XMFLOAT3(1.0f, -1.0f, -1.0f), XMFLOAT2(0.0f, 1.0f) },
		{ XMFLOAT3(1.0f, 1.0f, -1.0f), XMFLOAT2(0.0f, 0.0f) },
		{ XMFLOAT3(1.0f, 1.0f, 1.0f), XMFLOAT2(1.0f, 0.0f) },

		{ XMFLOAT3(-1.0f, -1.0f, -1.0f), XMFLOAT2(0.0f, 1.0f) },
		{ XMFLOAT3(1.0f, -1.0f, -1.0f), XMFLOAT2(1.0f, 1.0f) },
		{ XMFLOAT3(1.0f, 1.0f, -1.0f), XMFLOAT2(1.0f, 0.0f) },
		{ XMFLOAT3(-1.0f, 1.0f, -1.0f), XMFLOAT2(0.0f, 0.0f) },

		{ XMFLOAT3(-1.0f, -1.0f, 1.0f), XMFLOAT2(1.0f, 1.0f) },
		{ XMFLOAT3(1.0f, -1.0f, 1.0f), XMFLOAT2(0.0f, 1.0f) },
		{ XMFLOAT3(1.0f, 1.0f, 1.0f), XMFLOAT2(0.0f, 0.0f) },
		{ XMFLOAT3(-1.0f, 1.0f, 1.0f), XMFLOAT2(1.0f, 0.0f) },
	};

	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(BasicVertex) * 24;
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = 0;
	D3D11_SUBRESOURCE_DATA InitData;
	ZeroMemory(&InitData, sizeof(InitData));
	InitData.pSysMem = vertices;
	V_RETURN(pd3dDevice->CreateBuffer(&bd, &InitData, &m_pVertexBuffer));

	// Create test cube index buffer
	DWORD indices[] =
	{
		3,1,0,
		2,1,3,

		6,4,5,
		7,4,6,

		11,9,8,
		10,9,11,

		14,12,13,
		15,12,14,

		19,17,16,
		18,17,19,

		22,20,21,
		23,20,22
	};

	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(DWORD) * 36;
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bd.CPUAccessFlags = 0;
	bd.MiscFlags = 0;
	InitData.pSysMem = indices;
	V_RETURN(pd3dDevice->CreateBuffer(&bd, &InitData, m_pIndexBuffer.GetAddressOf()));

	// Create the constant buffers
	bd.Usage = D3D11_USAGE_DYNAMIC;
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	bd.ByteWidth = sizeof(FreqUpdatedCB);
	V_RETURN(pd3dDevice->CreateBuffer(&bd, nullptr, m_pFreqUpdateBuffer.GetAddressOf()));

	// Initialize the world matrices
	m_WorldMatrix = XMMatrixIdentity();

	// Setup the camera's view parameters
	static const XMVECTORF32 s_Eye = { 0.0f, 0.0f, -6.0f, 0.f };
	static const XMVECTORF32 s_At = { 0.0f, 0.0f, 0.0f, 0.f };
	m_modelViewCamera.SetViewParams(s_Eye, s_At);
	// Setup the camera's projection parameters
	float fAspectRatio = bbWidth / (float)bbHeight;
	m_modelViewCamera.SetProjParams(XM_PI / 4, fAspectRatio, 0.01f, 1000.0f);
	m_modelViewCamera.SetWindow(bbWidth, bbHeight);
	m_modelViewCamera.SetButtonMasks(MOUSE_LEFT_BUTTON, MOUSE_WHEEL, MOUSE_MIDDLE_BUTTON);

	// Create the sample state
	D3D11_SAMPLER_DESC sampDesc;
	ZeroMemory(&sampDesc, sizeof(sampDesc));
	sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	sampDesc.MinLOD = 0;
	sampDesc.MaxLOD = D3D11_FLOAT32_MAX;
	V_RETURN(pd3dDevice->CreateSamplerState(&sampDesc, m_pSamplerLinear.GetAddressOf()));

	// Create texture resources
	D3D11_TEXTURE2D_DESC	RTtextureDesc = { 0 };
	RTtextureDesc.Width = bbWidth;
	RTtextureDesc.Height = bbHeight;
	RTtextureDesc.MipLevels = 1;
	RTtextureDesc.ArraySize = 1;
	RTtextureDesc.Format = DXGI_FORMAT_R16G16B16A16_FLOAT;
	RTtextureDesc.SampleDesc.Count = 1;
	RTtextureDesc.Usage = D3D11_USAGE_DEFAULT;
	RTtextureDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
	RTtextureDesc.CPUAccessFlags = 0;
	RTtextureDesc.MiscFlags = 0;
	V_RETURN(pd3dDevice->CreateTexture2D(&RTtextureDesc, NULL, m_pTex2D.GetAddressOf()));

	D3D11_SHADER_RESOURCE_VIEW_DESC RTshaderResourceDesc;
	RTshaderResourceDesc.Format = RTtextureDesc.Format;
	RTshaderResourceDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	RTshaderResourceDesc.Texture2D.MostDetailedMip = 0;
	RTshaderResourceDesc.Texture2D.MipLevels = 1;
	V_RETURN(pd3dDevice->CreateShaderResourceView(m_pTex2D.Get(), &RTshaderResourceDesc, m_pSRV.GetAddressOf()));

	D3D11_RENDER_TARGET_VIEW_DESC	RTviewDesc;
	RTviewDesc.Format = RTtextureDesc.Format;
	RTviewDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
	RTviewDesc.Texture2D.MipSlice = 0;
	V_RETURN(pd3dDevice->CreateRenderTargetView(m_pTex2D.Get(), &RTviewDesc, m_pRTV.GetAddressOf()));

	// Create Stencil View
	D3D11_TEXTURE2D_DESC descDepth;
	ZeroMemory(&descDepth, sizeof(descDepth));
	descDepth.Width = bbWidth;
	descDepth.Height = bbHeight;
	descDepth.MipLevels = 1;
	descDepth.ArraySize = 1;
	descDepth.Format = DXUTGetDeviceSettings().d3d11.AutoDepthStencilFormat;
	descDepth.SampleDesc.Count = 1;
	descDepth.SampleDesc.Quality = 0;
	descDepth.Usage = D3D11_USAGE_DEFAULT;
	descDepth.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	descDepth.CPUAccessFlags = 0;
	descDepth.MiscFlags = 0;
	hr = pd3dDevice->CreateTexture2D(&descDepth, NULL, m_pDepthTex2D.GetAddressOf());

	D3D11_DEPTH_STENCIL_VIEW_DESC descDSV;
	ZeroMemory(&descDSV, sizeof(descDSV));
	descDSV.Format = descDepth.Format;
	descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	descDSV.Texture2D.MipSlice = 0;

	// Create Depth Stencil State
	D3D11_DEPTH_STENCIL_DESC dsDesc;
	// Depth test parameters
	dsDesc.DepthEnable = false; // Disable Depth Test
	dsDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	dsDesc.DepthFunc = D3D11_COMPARISON_ALWAYS;
	// Stencil test parameters
	dsDesc.StencilEnable = true;
	dsDesc.StencilReadMask = 0xFF;
	dsDesc.StencilWriteMask = 0xFF;
	// Stencil operations if pixel is front-facing
	dsDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	dsDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	// Stencil operations if pixel is back-facing
	dsDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	dsDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	V_RETURN(pd3dDevice->CreateDepthStencilState(&dsDesc, m_pDepthStencilState.GetAddressOf()));

	// Create the depth stencil view
	V_RETURN(pd3dDevice->CreateDepthStencilView(m_pDepthTex2D.Get(), // Depth stencil texture
		&descDSV, // Depth stencil desc
		m_pDepthStencilView.GetAddressOf()));

	// Setup ViewPort
	m_demoViewPort.TopLeftX = 0;
	m_demoViewPort.TopLeftY = 0;
	m_demoViewPort.MinDepth = 0.0f;
	m_demoViewPort.MaxDepth = 1.0f;
	m_demoViewPort.Width = bbWidth;
	m_demoViewPort.Height = bbHeight;

	// Load Obj Model
	//V_RETURN(m_objModelViewer.LoadObjModel(pd3dDevice, "ClinderModel.obj"));
	//V_RETURN(m_objModelLoader.LoadObjModel(pd3dDevice, "CubeModel.obj"));
	//V_RETURN(m_objModelViewer.LoadObjModel(pd3dDevice, "SphereModel.obj"));
	V_RETURN(m_objModelViewer.LoadObjModel(pd3dDevice, "Bunny.obj"));

	// Voxelize Obj
	V_RETURN(m_objVoxelizer.CreateResource(pd3dDevice, bbWidth, bbHeight));

	// Voxel Viewer
	V_RETURN(m_objVoxelViewer.CreateResource(pd3dDevice, bbWidth, bbHeight));

	m_bbWidth = bbWidth;
	m_bbHeight = bbHeight;
	m_pGlobalDevice = pd3dDevice;
	return hr;
}

void RefractiveObjRenderDemo::Render(ID3D11DeviceContext* pd3dImmediateContext, ID3D11ShaderResourceView* inputSRV)
{
	m_pGlobalImmediateContext = pd3dImmediateContext;

	ID3D11RasterizerState* rs = nullptr;
	pd3dImmediateContext->RSGetState(&rs);

	pd3dImmediateContext->ClearRenderTargetView(m_pRTV.Get(), Colors::MidnightBlue);
	pd3dImmediateContext->ClearDepthStencilView(m_pDepthStencilView.Get(), D3D11_CLEAR_DEPTH, 1.0, 0);
	pd3dImmediateContext->OMSetRenderTargets(1, m_pRTV.GetAddressOf(), m_pDepthStencilView.Get());

	pd3dImmediateContext->RSSetViewports(1, &m_demoViewPort);
	// Set the input layout
	pd3dImmediateContext->IASetInputLayout(m_pVertexLayout.Get());
	// Set vertex buffer/
	//UINT stride = sizeof(BasicVertex);
	UINT stride = m_objModelViewer.GetVertexStride();
	UINT offset = 0;
	//pd3dImmediateContext->IASetVertexBuffers(0, 1, &m_pVertexBuffer, &stride, &offset);
	pd3dImmediateContext->IASetVertexBuffers(0, 1, m_objModelViewer.GetVertexBuffer(), &stride, &offset);
	// Set index buffer
	//pd3dImmediateContext->IASetIndexBuffer(m_pIndexBuffer, DXGI_FORMAT_R32_UINT, 0);
	pd3dImmediateContext->IASetIndexBuffer(*m_objModelViewer.GetIndexBuffer(), DXGI_FORMAT_R32_UINT, 0);
	// Set primitive topology
	pd3dImmediateContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	XMMATRIX mWorldViewProjection = m_WorldMatrix * m_modelViewCamera.GetViewMatrix() * m_modelViewCamera.GetProjMatrix();
	HRESULT hr;
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	V(pd3dImmediateContext->Map(m_pFreqUpdateBuffer.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource));
	auto pCB = reinterpret_cast<FreqUpdatedCB*>(mappedResource.pData);
	XMStoreFloat4x4(&pCB->mWorldViewProj, XMMatrixTranspose(mWorldViewProjection));
	XMStoreFloat4x4(&pCB->mWorld, XMMatrixTranspose(m_WorldMatrix));
	pCB->vMeshColor = XMFLOAT4(1.0f, 0.0f, 1.0f, 1.0f);
	pd3dImmediateContext->Unmap(m_pFreqUpdateBuffer.Get(), 0);

	// Render the cube
	pd3dImmediateContext->VSSetShader(m_pVertexShader.Get(), nullptr, 0);
	//pd3dImmediateContext->VSSetShader(m_pVertexShader, nullptr, 0);
	pd3dImmediateContext->VSSetConstantBuffers(0, 1, m_pFreqUpdateBuffer.GetAddressOf());
	pd3dImmediateContext->PSSetShader(m_pPixelShader.Get(), nullptr, 0);
	pd3dImmediateContext->PSSetConstantBuffers(0, 1, m_pFreqUpdateBuffer.GetAddressOf());
	pd3dImmediateContext->PSSetShaderResources(0, 1, &inputSRV);
	pd3dImmediateContext->PSSetSamplers(0, 1, m_pSamplerLinear.GetAddressOf());
	//pd3dImmediateContext->DrawIndexed(36, 0, 0);
	pd3dImmediateContext->DrawIndexed(m_objModelViewer.GetIndicesCount(), 0, 0);

	ID3D11VertexShader* tmpVertexShader = nullptr;
	ID3D11PixelShader* tmpPixelShader = nullptr;
	ID3D11Buffer* tmpBuffer = nullptr;
	ID3D11ShaderResourceView* tmpSRV = nullptr;
	ID3D11SamplerState* tmpSampler = nullptr;
	pd3dImmediateContext->VSSetShader(tmpVertexShader, nullptr, 0);
	pd3dImmediateContext->VSSetConstantBuffers(0, 1, &tmpBuffer);
	pd3dImmediateContext->PSSetShader(tmpPixelShader, nullptr, 0);
	pd3dImmediateContext->PSSetConstantBuffers(0, 1, &tmpBuffer);
	pd3dImmediateContext->PSSetShaderResources(0, 1, &tmpSRV);
	pd3dImmediateContext->PSSetSamplers(0, 1, &tmpSampler);

	// Restore the blend state
	pd3dImmediateContext->OMSetBlendState(0, 0, 0xffffffff);
	pd3dImmediateContext->RSSetState(rs);
	SAFE_RELEASE(rs);
}

void RefractiveObjRenderDemo::BeginVoxelization(ID3D11DeviceContext* pd3dImmediateContext)
{
	//if (!m_bPerformVoxelization) return;
	//m_bPerformVoxelization = false;
	m_objVoxelizer.Voxelization(pd3dImmediateContext, &m_objModelViewer);
}

void RefractiveObjRenderDemo::VisulizeVoxels(ID3D11DeviceContext* pd3dImmediateContext)
{
	uint32_t w = m_objVoxelizer.GetVolumeWidth();
	uint32_t h = m_objVoxelizer.GetVolumeHeight();
	uint32_t d = m_objVoxelizer.GetVolumeDepth();
	XMFLOAT3 minPt = m_objModelViewer.GetAABBMin(), maxPt = m_objModelViewer.GetAABBMax();
	XMFLOAT4 volumeScales = XMFLOAT4((maxPt.x - minPt.x) / w, (maxPt.y - minPt.y) / h, (maxPt.z - minPt.z) / d, 1.0f);
	m_objVoxelViewer.RenderModel(pd3dImmediateContext, &m_objModelViewer);
	m_objVoxelViewer.RenderVoxels(pd3dImmediateContext, m_objVoxelizer.GetVolumeSRV(), w, h, d, volumeScales);
}

void RefractiveObjRenderDemo::UpdateCamera(float timeElapsed)
{
	// Update the camera's position based on user input 
	m_modelViewCamera.FrameMove(timeElapsed);

	// Update Viewer's Camera
	m_objVoxelViewer.UpdateCamera(timeElapsed);
}

//*******************************
// GUI Implementation
//*******************************
void RefractiveObjRenderDemo::RenderGUI()
{
	if (ImGui::CollapsingHeader("Voxelization Parameters"))
	{
		ImGui::BulletText("MSAA Sampling Count");
		//ImGui::Indent();
		ImGui::Separator();
		enum Mode
		{
			One,
			Two,
			Four,
			Eight
		};
		static int mode = 0;
		if (ImGui::RadioButton("One", mode == One))
		{
			mode = One;
			m_objVoxelizer.SetMSAASampleCount(1);
			m_objVoxelizer.CreateRenderTargetResource(m_pGlobalDevice.Get());
			m_objVoxelizer.Voxelization(m_pGlobalImmediateContext.Get(), &m_objModelViewer);
		}
		ImGui::SameLine();
		if (ImGui::RadioButton("Two", mode == Two))
		{
			mode = Two;
			m_objVoxelizer.SetMSAASampleCount(2);
			m_objVoxelizer.CreateRenderTargetResource(m_pGlobalDevice.Get());
			m_objVoxelizer.Voxelization(m_pGlobalImmediateContext.Get(), &m_objModelViewer);
		}
		ImGui::SameLine();
		if (ImGui::RadioButton("Four", mode == Four))
		{
			mode = Four;
			m_objVoxelizer.SetMSAASampleCount(4);
			m_objVoxelizer.CreateRenderTargetResource(m_pGlobalDevice.Get());
			m_objVoxelizer.Voxelization(m_pGlobalImmediateContext.Get(), &m_objModelViewer);
		}
		ImGui::SameLine();
		if (ImGui::RadioButton("Eight", mode == Eight))
		{
			mode = Eight;
			m_objVoxelizer.SetMSAASampleCount(8);
			m_objVoxelizer.CreateRenderTargetResource(m_pGlobalDevice.Get());
			m_objVoxelizer.Voxelization(m_pGlobalImmediateContext.Get(), &m_objModelViewer);
		}

		ImGui::Separator();

		ImGui::BulletText("MSAA Sampling Quality");
		ImGui::Separator();
		static float msaaSampleQuality = 0.5f;
		ImGui::SliderFloat("Quality Value", &msaaSampleQuality, 0.0f, 1.0f, "Quality Value = %.3f");
		ImGui::Separator();
	}
}